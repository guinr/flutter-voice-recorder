import 'package:flutter/material.dart';
import 'package:simple_permissions/simple_permissions.dart';
import 'package:speech_recognition/speech_recognition.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.amber,
      ),
      home: VoiceHome(),
    );
  }
}

class VoiceHome extends StatefulWidget {
  @override
  _VoiceHomeState createState() => _VoiceHomeState();
}

class _VoiceHomeState extends State<VoiceHome> {
  SpeechRecognition _speechRecognition;
  bool _isAvailable = false;
  bool _isListening = false;

  String resultText = "";

  @override
  void initState() {
    super.initState();
    _checkAudioPermission();
    initSpeechRecognizer();
  }

  void _checkAudioPermission() async {
    bool hasPermission =
    await SimplePermissions.checkPermission(Permission.RecordAudio);
    if (!hasPermission) {
      await SimplePermissions.requestPermission(Permission.RecordAudio);
    }
  }

  void initSpeechRecognizer() {
    _speechRecognition = SpeechRecognition();

    _speechRecognition.setAvailabilityHandler(
      (bool result) => setState(() => _isAvailable = result),
    );

    _speechRecognition.setRecognitionStartedHandler(
      () => setState(() => _isListening = true),
    );

    _speechRecognition.setRecognitionResultHandler(
      (String speech) => setState(() => resultText = (speech == "melão" ? "Speek friend and enter =)" : speech)),
    );

    _speechRecognition.setRecognitionCompleteHandler(
      (String text) => setState(() => _isListening = false),
    );

    _speechRecognition.setErrorHandler(() => initSpeechRecognizer());

    _speechRecognition.activate().then(
      (result) => setState(() => _isAvailable = result),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              padding: EdgeInsets.symmetric(vertical: 8, horizontal: 12),
              child: Text(_isListening ? "Ouvindo": ""),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                FloatingActionButton(
                  child: Icon(Icons.cancel),
                  mini: true,
                  onPressed: () {
                    if (_isListening) {
                      _speechRecognition.cancel().then(
                        (result) => setState(() {
                          _isListening = result;
                          resultText = "";
                        }),
                      );
                    }
                  },
                ),
                FloatingActionButton(
                  child: Icon(Icons.mic),
                  backgroundColor: Colors.indigoAccent,
                  onPressed: () {
                    if (_isAvailable && !_isListening) {
                      _speechRecognition
                        .listen(locale: "pt_BR")
                        .then((result) => print('$result'));
                    }
                  },
                ),
                FloatingActionButton(
                  child: Icon(Icons.stop),
                  mini: true,
                  onPressed: () {
                    if (_isListening) {
                      _speechRecognition.stop().then(
                        (result) => setState(() => _isListening = result),
                      );
                    }
                  },
                ),
              ],
            ),
            Container(
              width: MediaQuery.of(context).size.width * 0.8,
              padding: EdgeInsets.symmetric(vertical: 8, horizontal: 12),
              margin: EdgeInsets.all(15),
              decoration: BoxDecoration(
                color: Colors.blue,
                borderRadius: BorderRadius.circular(3),
              ),
              child: Text(resultText),
            )
          ],
        ),
      ),
    );
  }
}
